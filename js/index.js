$(function () {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$('.carousel').carousel({
		interval: 6000
	})
});

$('#suscripcion-mod').on('show.bs.modal',function(e) {
	console.log('Mostrando la modal');
	$('#suscripcion-btn').removeClass('btn-outline-success');
	$('#suscripcion-btn').addClass('btn-primary');
	$('#suscripcion-btn').prop('disabled',true);
});
$('#suscripcion-mod').on('shown.bs.modal',function(e) {
	console.log('Se mostró la modal');
});
$('#suscripcion-mod').on('hide.bs.modal',function(e) {
	$('#suscripcion-btn').removeClass('btn-primary');
	$('#suscripcion-btn').addClass('btn-outline-success');
	$('#suscripcion-btn').prop('disabled',false);
});
$('#suscripcion-mod').on('hidden.bs.modal',function(e) {
	console.log('Se cerró la modal');
});