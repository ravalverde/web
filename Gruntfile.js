module.exports = function (grunt) {
	const sass = require('node-sass');

	require('jit-grunt')(grunt, {
		useminPrepare: 'grunt-usemin'
	});
	require('time-grunt')(grunt);

	grunt.initConfig({

		sass: {
			options: {
				implementation: sass,
				sourceMap: true
			},
			dist: {
				files: {
					'CSS/styles.css': 'CSS/styles.scss'
				}
			}
		},
		watch: {
			css: {
				files: ['CSS/*.scss'],
				tasks: ['sass']
			}		
		},

		browserSync: {
			dev: {
				bsFiles: {
					src : [
					'CSS/*.css',
					'*.html',
					'js/*.js'
					]
				},
				options: {
					watchTask: true,
					server: './'
				}
			}
		},
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: './',
					src: ['Images/*.{png,jpg,gif}'],
					dest: 'dist/Images'
				}]
			}
		},
		copy: {
			files: {
    			cwd: 'node_modules/open-iconic/font/fonts/',  // set working folder / root to copy
    			src: '**/*',           // copy all files and subfolders
    			dest: 'dist/font/fonts',    // destination folder
    			expand: true           // required when using cwd
    		},
    		html: {
    			cwd: './',  // set working folder / root to copy
    			src: '*.html',           // copy all files and subfolders
    			dest: 'dist',    // destination folder
    			expand: true,          // required when using cwd
    			dot: true
    		}
    	},
    	clean: {
    		build: {
    			src: ['dist/']
    		}
    	},

    	cssmin: {
    		dist: {}
    	},

    	uglify: {
    		dist: {}
    	},
    	
    	
    	filerev: {
    		options: {
    			algorithm: 'md5',
    			length: 20
    		}
    		,
    		realese: {
    			files: [{
    				src: ['dist/js/*.js' ,
    				'dist/CSS/*.css' ]
    			}]
    		}
    	},
    	concat :{
    		options:{
    			separator: ';'
    		},
    		dist: {}
    	},

    	useminPrepare: {
    		foo: {
    			dest: 'dist',
    			src: ['index.html', 'precios.html', 'contacto.html','help.html']
    		},
    		options: {
    			flow: {
    				steps: {
    					css: ['cssmin'],
    					js: ['uglify']
    				},
    				post: {
    					css: [{
    						name: 'cssmin',
    						createConfig: function(context,block) {
    							var generated = context.options.generated;
    							generated.options = {
    								keepSpecialComments: 0,
    								rebase: false
    							}
    						}
    					}]
    				}

    			}
    		}
    	},
    	usemin: {
    		html: ['dist/index.html', 'dist/precios.html', 'dist/help.html', 'dist/contacto.html'],
    		options: {
    			assetsDir: ['dist','dis/CSS','dist/js']
    		}
    	}


    });

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	grunt.registerTask('default', ['browserSync', 'watch']);
	grunt.registerTask('build', 
		['clean', 
		'copy',
		'imagemin',
		'cssmin',
		'uglify',
		'filerev',
		'usemin']);
	grunt.registerTask('copiar',['copy']);
	grunt.registerTask('imagen',['imagemin']);
	grunt.registerTask('css',['sass']);

};
